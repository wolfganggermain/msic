package com.example.root.finalmsic;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
import android.content.Context;
import android.graphics.Typeface;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;
/**
 * Created by wolfgang on 2016/07/01.
 */
public class CustomAdapter extends DataAssembly {
    /*Expandable adapter logic to be reviewed todo*/


    MediaPlayer mp;
    private Context ctx;
    private HashMap<String, List<String>> vTypeAMT;//categories
    private List<String> vTrack;
    private Bitmap displayImg;
    private ArrayList<String> vTrackName;
    private int ind = 0;
    private int total =2;
    private Integer resID;



    public CustomAdapter(Context ctx, HashMap<String, List<String>> vTypeAMT){//}, Bitmap displayImg){
        this.ctx = ctx;
        this.vTypeAMT = vTypeAMT;
        MediaPlayer lmp = new MediaPlayer();



        mp = getMp(vTypeAMT);
        this.displayImg = displayImg;
        //vTrackName[0] = "Track 01";
       // vTrackName[1] = "Track 02";
        //String playSoundName = "max_schneider";



/*
        allMusic = MediaPlayer.create(ctx, R.raw.max_schneider);
        allMusic = MediaPlayer.create(ctx, R.raw.nice_beat);
        allMusic = MediaPlayer.create(ctx, R.raw.sia_cheap_thrill);
*/
    }



    ///implement code not coded
    @Override
    public int getGroupCount() {


        return vTypeAMT.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return vTypeAMT.get(vTrack.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return vTrack.get(groupPosition);
    }

    @Override
    public Object getChild(int parent, int child) {
        return vTypeAMT.get(vTrack.get(parent)).get(child);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int parent, int child) {
        return child;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int parent, boolean isExpanded, View convertView, ViewGroup parentView) {
        /*final ArrayList<Integer> allResID = getLoadedMp(ctx, vTrack);
      //  String groupTitle =(String) getGroup(parent);
        isExpanded = true;
        */

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.parent_layout, parentView, false);
        }
        ImageView ivMusicVisualid = (ImageView) convertView.findViewById(R.id.ivMusicVisualid);
        ListView lvParent = (ListView)convertView.findViewById(R.id.lvParent);
        ivMusicVisualid.setImageResource(R.drawable.visualid);


        String albumTitle = getAlbumTitle();


        this.vTrack = getTrack(albumTitle);



        /*****************************************Work to be done here to improve player *****************************/



        final ToggleButton ibPlay = (ToggleButton) convertView.findViewById(R.id.ibPlay);
        ibPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean resumeCheck = false;



                /*resID = allResID.get(ind);*/

                if(ibPlay.isChecked()) {
                    //ibPlay.setBackgroundResource(R.drawable.player);
                   /* if(resumeCheck){
                        mp.start();

                    }*/




                    mp = MediaPlayer.create(ctx, resID);
                    mp.start();
                    ibPlay.setBackgroundResource(R.drawable.black);

                }else{
                    ibPlay.setBackgroundResource(R.drawable.player);
                    Toast.makeText(ctx, "Pause is on", Toast.LENGTH_SHORT).show();
                    mp.pause();
                    resumeCheck = true;



                }

               // ind++;//might be causing error

               // }catch (IOException ioe){
                  // Toast.makeText(ctx, "This error occured" + ioe, Toast.LENGTH_SHORT).show();

                //}

            }
        });

        ibPlay.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                /**Need a long click or other method that is good to add another functionality to the play button*/


                mp.stop();

                return false;
            }
        });

        final ImageButton ibNext = (ImageButton) convertView.findViewById(R.id.ibRight);
        ibNext.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                if(!(ind==total)) {
                    mp.stop();
                    Toast.makeText(ctx, "index is at: " + ind + " Red ID is: " + resID, Toast.LENGTH_SHORT).show();
                    ind++;


                    resID++;
                    //ind =resID;


                    mp = MediaPlayer.create(ctx, resID);
                    //mp.selectTrack(ind);
                    mp.start();

                }else{
                    Toast.makeText(ctx, "Last track reached", Toast.LENGTH_SHORT).show();

                }
            }
        });

        final ImageButton ibPrev = (ImageButton) convertView.findViewById(R.id.ibLeft);
        ibPrev.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {

               // if(ind <=0){
                ind--;
                    Toast.makeText(ctx, "On first track already", Toast.LENGTH_SHORT).show();
                mp.stop();
                /*resID  = allResID.get(ind);*/
                mp = MediaPlayer.create(ctx, resID);
                //mp.selectTrack(ind);
                mp.start();



               // }
            }
        });

















        final ImageButton ibMP = (ImageButton) convertView.findViewById(R.id.ibMP);
        ibMP.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
              // Intent i = new Intent(new CustomMediaPlayer() );

            }
        });






        /*Hardcode testing
            temp[0] = " Track 01";
            temp[1] = " Track 02";

        //issues lies with vTrack being empty look up cause.*/


        /*************************************End******************************************/







        LVAdapter adpater = new LVAdapter(convertView.getContext(), vTrack);
        lvParent.setAdapter(adpater);





        /*TextView parentTV = (TextView) convertView.findViewById(R.id.tvName);
        ImageView ivParent = (ImageView)convertView.findViewById(R.id.ivDp);
        ivParent.setImageResource(R.drawable.placeholder);

        parentTV.setTypeface(null, Typeface.BOLD);//making text bold
        parentTV.setText(groupTitle);*/
        return convertView;
    }

    @Override
    public View getChildView(int parent, int child, boolean lastChild, View convertView, ViewGroup parentView) {
        String childTitle = (String) getChild(parent, child);
        //create the convert view here
        if(convertView == null){
            LayoutInflater inflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflator.inflate(R.layout.child_layout, parentView, false);
        }
        ImageView ivMusicVisualid = (ImageView) convertView.findViewById(R.id.ivMusicVisualid);
        ListView lvParent = (ListView)convertView.findViewById(R.id.lvChild);

        ivMusicVisualid.setImageResource(R.drawable.visualid);

        LVAdapter adpater = new LVAdapter(convertView.getContext(), vTrackName);
        lvParent.setAdapter(adpater);




        /*TextView childTV = (TextView) convertView.findViewById(R.id.child_txt);
        childTV.setBackgroundColor(Color.WHITE);
        childTV.setPadding(30, 30, 30, 30);
        childTV.setText(childTitle);*/
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
    //coded


}
