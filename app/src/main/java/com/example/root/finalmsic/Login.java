package com.example.root.finalmsic;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by root on 2016/07/17.
 */
public class Login extends Activity {
    //instance variable
    private String uname, password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
    }



    //default construtor
    public Login(){
        uname = "";//username
        password = "";

    }


    //non-default
    public Login(String uname, String password){
        this.uname = uname;
        this.password = password;

    }

    //accessor
    public String getUname(){
        return uname;

    }

    public String getPassword(){
        return password;

    }
    /*//dont want this to be changed or now but logic to b decided later
    //mutator
    public void setUName(String uname){
        this.uname = uname;

    }


    public void setPassword(String password){
        this.password = password;

    }
    */

    //toString

    public String toString(){
        return "";//to be coded later if there is a neeed

    }





}
