package com.example.root.finalmsic;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;


public class CustomMediaPlayer extends Activity {
private Button btPlay, btPause, btStop;
private SeekBar seekBar2;
private TextView tvSeek;
private double duration;
private int timeInSecond;
private int second, minute;

// private MediaPlayer mp;

@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.media_player_fs);
        btPlay = (Button)findViewById(R.id.btPlay);
        btPause = (Button)findViewById(R.id.btPause);
        btStop = (Button)findViewById(R.id.btStop);

//final MediaPlayer mp = MediaPlayer.create(this, R.raw.song1);
        /*
        * Testing for one song so far
        *
        * */

      //  duration = getDuration(mp);


        //adding actionlistern
      btPlay.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
              //  mp.start();
        }
        });//thinking of using a toggle instead of a normal button, to remove the need for stop


        btPause.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //.pause();
            }
        });

        btStop.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
       // mp.stop();
                CustomMediaPlayer.this.finish(); //end app back to main screen
        }
        });
        //end of listener
        seekBarr();



        }


public void seekBarr(){
        seekBar2 =(SeekBar)findViewById(R.id.seekBar);
        tvSeek =(TextView)findViewById(R.id.tvSeek);
        timeInSecond = (int) duration/1000;
        second = timeInSecond%60;
        minute = timeInSecond/60;


        tvSeek.setText("Time: " + seekBar2.getProgress() + "/ " + minute+":"+second);

        seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        int progress1;
        String totalTime = minute + ":" + second;


@Override
public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        //progreess is of seekbar
        this.progress1=progress;
        int inMin = (int) (((progress/60)*(3.10)));//conversion issue here
        int inSec = (int)(((progress%60)*(3.10))); //try and find a better way of getting the time


        tvSeek.setText("Time : " + inMin+":"+inSec + "/ " + totalTime);
        //Toast.makeText(MainActivity.this, "Seekbarin progress:: seconds" + minute + ":" + second, Toast.LENGTH_SHORT).show();//for testing

        }

@Override
public void onStartTrackingTouch(SeekBar seekBar) {

        }

@Override
public void onStopTrackingTouch(SeekBar seekBar) {
        //  tvSeek.setText("Covered : " + inMin+":"+inSec + "/ " + totalTime);
        //Toast.makeText(MainActivity.this, "Stop tracking", Toast.LENGTH_SHORT).show();
        }
        });


        }


public double getDuration(MediaPlayer mp){
        return mp.getDuration();



        }


public String getTimeString(){
        //thinking f using this as place that provides the time

        return "";
        }

        }