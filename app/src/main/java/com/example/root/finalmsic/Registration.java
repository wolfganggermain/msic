package com.example.root.finalmsic;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by root on 2016/07/17.
 */
public class Registration extends Activity {
    ////instance variable - i DECLARED all my variables here
    private String name, surname, uname, email, password;
    private int DoB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
    }




    //default constructor
    public Registration(){
        //initialise it to a default value
        name = "";
        surname = "";
        uname = "";//username
        email = "";
        password = "";
        DoB = 0;





    }

    //non-default construtor
    public Registration(String name, String surname, String email,String uname, String pw, int DoB){ // this it the order i'lluse them in to make an object
        this.name = name; //this instance variable "name" will be assigned with the name of the parameter
        this.surname = surname;
        this.email = email;
        this.uname = uname;
        password = pw; // if the name are different you can use the name without this keyword
        this.DoB = DoB;



    }


    //accessor
    public String getName(){
        String lName = new String(name);  //l for local

        return lName;

    }


    public String getSurnameName(){
        String lSName = new String(surname);
        return lSName;

    }


    public String getEmail(){
        String lEmail = new String(email);
        return lEmail;

    }

    public String getUName(){
        String lUname = new String(uname);
        return uname;

    }

    public String getPassword(){
        String lPass = new String(password);
        return password;//ENCRYPTION I NEEDED HERE

    }


    public int getDOB(){
        return DoB; // since int is a premitive no need to apply above rules

    }

    //mutator


    public void setName(String name){
        this.name = name;

    }


    public void setSurname(String surname){
        this.surname = surname;

    }


    public void setEmail(String email){
        this.email = email;

    }


    public void setUName(String uname){
        this.uname = uname;

    }


    public void setPassword(String pw){ // maintaining converntion
        password = pw;

    }


    public void setName(int BoD){
        this.DoB = BoD;

    }


    //toString

    public String toString(){
        return "name: " + name +  "surname: " + surname + " email: " + email;
    }



    ///normal code now








}
