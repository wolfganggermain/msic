package com.example.root.finalmsic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;

import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity
        implements NavigationView.OnNavigationItemSelectedListener {

    private HashMap<String, List<String>> vTypeAMT;
    private List<String> vTrack;
    private ExpandableListView expList;
    private CustomAdapter adpater;
    private Button secret;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    public static final String myPREFERNCES = "myPrefs";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /****************Working on expandable list view*********************/
        expList = (ExpandableListView) findViewById(R.id.exp_list);
        vTypeAMT = DataProvider.getInfo(MainActivity.this);//because it is static we can call it
        //secret = (Button)findViewById(R.id.secret);
        //vTrack = new ArrayList<String>();

       /* for(int i=0; i<vTypeAMT.size(); i++) {
            vTrack.add(vTypeAMT.get(al));
        }
            */
      // vTrack = new ArrayList<String>(DataProvider.getInfo(this).keySet());//get key set from the hash map

        /*secret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iinent = new Intent(MainActivity.this, Deal.class);
                startActivity(iinent);

            }
        });*/

        //adding adapter
        adpater = new CustomAdapter(MainActivity.this, vTypeAMT);
        expList.setAdapter(adpater);




        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       //setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.studio) {

           // FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
           // fragmentTransaction.replace(R.id.containerView,new CustomMediaPlayer()).addToBackStack(null).commit();
            // Handle the camera action
        } else if (id == R.id.fs_mediaplayer) {
            Intent i = new Intent("com.example.root.finalmsic.CUSTOMMEDIAPLAYER");
            startActivity(i);

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.register) {
            Intent i = new Intent("com.example.root.finalmsic.REGISTRATION");
            startActivity(i);

        } else if (id == R.id.nav_send) {
            Intent i = new Intent("com.example.root.finalmsic.LOGIN");
            startActivity(i);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public static void addSong(){



    }


  /*  public MediaPlayer getSongs(){


        //return allMusic;
    }*/
}
