package com.example.root.finalmsic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wolfgang on 2016/07/10.
 */
public class LVAdapter extends ArrayAdapter<String> {
    private ArrayList<String> vTrack;
    //private String[] dates;

    public LVAdapter(Context context, List<String> vTrack) { //2nd paramter is what you need to pass in
        super(context, R.layout.list_layout, vTrack); //middle paramter for adding custom row
        this.vTrack = new ArrayList<>(vTrack);//coverting back to array for normal usage
        //dates = date.clone();


    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater wolfInflator = LayoutInflater.from(getContext());
        View customView = wolfInflator.inflate(R.layout.list_layout, parent, false);

        //for reuses
        int usePos = position;


        //This is what was used to do the comparison
        String eventItem = getItem(usePos);//pos of item in list
        //String dateS  = dates[usePos];//S for string
        //end


        //All initialization was done here
        ImageView ivTrackIcon = (ImageView) customView.findViewById(R.id.ivTrackIcon);
        TextView tvTrackName = (TextView) customView.findViewById(R.id.tvTrackName);


        ivTrackIcon.setImageResource(R.drawable.music_track);
        tvTrackName.setText(eventItem);
        //end

        //control flow will be done here

        return customView;
    }

}
